# Copyright 1999-2004 Gentoo Foundation
# Distributed under the terms of the GNU General Public License v2
# $Header: $

inherit eutils

DESCRIPTION="Program for changing the resolution of an unavailable VBIOS mode on Intel 915 cards"
HOMEPAGE="http://www.geocities.com/stomljen/"
SRC_URI="http://www.geocities.com/stomljen/${PN}-${PV}.tar.gz"
LICENSE="as-is"
SLOT="0"
KEYWORDS="~x86"
IUSE=""
DEPEND=""
RESTRICT="nomirror"
S=${WORKDIR}/${PN}-${PV}

src_compile() {
	emake || die "emake failed"
}

src_install() {
	install -m755 -D 915resolution ${D}/usr/sbin/915resolution \
	|| die "install failed"
	dodoc README.txt
	
	insinto /etc/conf.d
	newins "${FILESDIR}/${PATCHLEVEL}/conf.d" 915resolution

	exeinto /etc/init.d
	newexe "${FILESDIR}/${PATCHLEVEL}/init.d" 915resolution
}

pkg_postinst() {
	einfo ""
	einfo "Edit your the config file in /etc/conf.d as you need"
	einfo "and add 915resolution to the default runlevel."
	einfo ""
}

