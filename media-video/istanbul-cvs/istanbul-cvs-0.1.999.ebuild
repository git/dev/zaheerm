# Copyright 1999-2006 Gentoo Foundation
# Distributed under the terms of the GNU General Public License v2
# $Header: /var/cvsroot/gentoo-x86/media-plugins/gst-plugins-libmms/gst-plugins-libmms-0.10.0.ebuild,v 1.2 2006/04/04 15:04:40 mr_bones_ Exp $

inherit gnome2 eutils cvs

ECVS_ANON="yes"
ECVS_CVS_OPTIONS="-dP"
ECVS_SERVER="anoncvs.gnome.org:/cvs/gnome"
ECVS_MODULE="istanbul"
ECVS_BRANCH="HEAD"

KEYWORDS="~x86"

RDEPEND=">=media-libs/gst-plugins-base-0.10.8
	dev-lang/python
	>=dev-python/pygtk-2.6
	>=dev-python/gnome-python-extras-2.11.3
	>=dev-python/gst-python-0.10.0
	media-plugins/gst-plugins-ogg
	media-plugins/gst-plugins-theora
	>=media-libs/libtheora-1.0_alpha6"

DEPEND="${RDEPEND}"
SRC_URI=""
S="${WORKDIR}/${ECVS_MODULE}"

src_unpack() {
	cvs_src_unpack
}

src_compile() {
	mkdir -p "${T}/home"
	export HOME="${T}/home"
	export GST_REGISTRY=${T}/home/registry.cache.xml
	addpredict /root/.gconfd
	addpredict /root/.gconf
	cd ${S}
	./autogen.sh || die "autogen.sh failed"
	econf
	emake
}
