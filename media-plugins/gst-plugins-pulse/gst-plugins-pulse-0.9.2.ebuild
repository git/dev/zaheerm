# Copyright 1999-2006 Gentoo Foundation
# Distributed under the terms of the GNU General Public License v2
# $Header: /var/cvsroot/gentoo-x86/media-plugins/gst-plugins-ffmpeg/gst-plugins-ffmpeg-0.10.1-r1.ebuild,v 1.1 2006/06/19 13:05:04 zaheerm Exp $

inherit flag-o-matic eutils

MY_PN=${PN/-plugins/}
MY_P=${MY_PN}-${PV}

# Create a major/minor combo for SLOT
PVP=(${PV//[-\._]/ })
SLOT=0.10
#SLOT=0.10

DESCRIPTION="GStreamer plugin for pulse audio"
HOMEPAGE="http://www.pulseaudio.org"
SRC_URI="http://0pointer.de/lennart/projects/${MY_PN}/${MY_P}.tar.gz"

LICENSE="GPL-2"
KEYWORDS="~amd64 ~x86"
IUSE=""

S=${WORKDIR}/${MY_P}

RDEPEND=">=media-libs/gstreamer-0.10.0
	>=media-libs/gst-plugins-base-0.10.0
	media-sound/pulseaudio"

DEPEND="${RDEPEND}
	dev-util/pkgconfig"

src_compile() {

	econf --disable-lynx || die
	emake || die

}

src_install() {

	make DESTDIR=${D} install || die

	dodoc AUTHORS COPYING ChangeLog INSTALL NEWS README TODO

}

