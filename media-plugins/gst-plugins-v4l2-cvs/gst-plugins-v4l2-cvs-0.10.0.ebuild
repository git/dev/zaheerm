# Copyright 1999-2006 Gentoo Foundation
# Distributed under the terms of the GNU General Public License v2
# $Header: /var/cvsroot/gentoo-x86/media-plugins/gst-plugins-libmms/gst-plugins-libmms-0.10.0.ebuild,v 1.2 2006/04/04 15:04:40 mr_bones_ Exp $

inherit gst-plugins-bad eutils cvs

ECVS_ANON="yes"
ECVS_CVS_OPTIONS="-dP"
ECVS_SERVER="anoncvs.freedesktop.org:/cvs/gstreamer"
ECVS_MODULE="gst-plugins-bad"
ECVS_BRANCH="HEAD"
GST_PLUGINS_BUILD="v4l2"
GST_PLUGINS_BUILD_DIR="v4l2"

KEYWORDS="~x86"

RDEPEND=">=media-libs/gst-plugins-base-0.10.8
	virtual/os-headers"

DEPEND="${RDEPEND}"
SRC_URI=""
S="${WORKDIR}/${ECVS_MODULE}"

src_unpack() {
	cvs_src_unpack
}

src_compile() {
	 cd ${S}
	 ./autogen.sh || die "autogen.sh failed"
	 gst_plugins_good_src_compile
}
